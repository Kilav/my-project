/*
 Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата".
 Создать в объекте вложенный объект - "Приложение".
 Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата".
 Создать методы для заполнения и отображения документа.
*/
const Document = {
    heading: "Заголовок",
    body: "Тіло",
    footer: "Футер",
    date: "Дата",
    application: {
        heading: "Заголовок",
        body: "Тіло",
        footer: "Футер",
        date: "Дата"
    },
    setDocument: function () {
        for (let element in this) {
            if ((element != "application") & (element != "setDocument") & (element != "show")) {
                this[element] = prompt(`Вкажіть ${Document[element]} документа: `, element);
            }
        }
        for (let element in this.application) {
            if (element != "date") {
                this.application[element] = prompt(`Вкажіть ${this.application[element]} програми: `, element);
            } else {
                this.application[element] = prompt(`Вкажіть ${this.application[element]} програми: `, new Date().toLocaleDateString());
            }
        }
    },
    show: function () {
        for (let element in this) {
            if ((element != "application") & (element != "setDocument") & (element != "show")) {
                document.write(`<p> ${element} документа: ${Document[element]}`);
            }
        }
        for (let element in this.application) {
            document.write(`<p> ${element} програми: ${Document.application[element]}`);
        }
    }
}
Document.setDocument();
Document.show();